import styled from 'styled-components';

export const Input = styled.input`
  padding: 6px 6px;
  display: block;
  border: 1px solid transparent;
`
export const Item = styled.div`
  flex-basis: 50%;
  &:nth-child(1){
    border-right: 1px solid black;
  }
  h2{
    text-align: center;
    border-bottom: 1px solid black;
    padding-bottom: 16px;
  }

`
export const Wrapper = styled.div`
  max-height: 500px;
  overflow-y: auto;
    div{
    width: 260px;
    display: flex;
    margin: 0 0 20px 20px;
    justify-content: space-evenly;
    span{
      font-weight: bold;
      font-size: 22px;
    }
  }
`