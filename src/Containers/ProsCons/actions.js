
export const storeInputValue = (e, key, inputValues, componentType) => dispatch => {
  const inputValue = e.target.value;

  if(inputValue.length > inputValues[key].length){
    dispatch({
      type: 'STORE_INPUT_VALUE',
      data: inputValue,
      componentType,
      key
    })
    if(inputValue.length===1){
      dispatch({
        type: 'ADD_INPUT',
      })
    }
  }
  else{
    dispatch({
      type: 'DELETE_INPUT',
      data: inputValue,
      componentType,
      key
    })
  } 
}