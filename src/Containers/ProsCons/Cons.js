import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { storeInputValue } from './actions';
import * as S from '../Style';

export const Cons = () => {
  const dispatch = useDispatch();

  const mainState = useSelector(state => state.mainState);
  const { initialValuesCons } = mainState;

  return(
    <S.Item>  
      <h2>Cons</h2>
      <S.Wrapper>
        { initialValuesCons && initialValuesCons.map((el, index) => (
          <div key={index}>
            <span>{`${index+1}.`}</span>
            <S.Input 
              value={initialValuesCons[index]}
              key={index}
              onChange={(e) => dispatch(storeInputValue(e, index, initialValuesCons, "cons"))}
               />
          </div>
          ))
        }
      </S.Wrapper>
    </S.Item> 
  )
}