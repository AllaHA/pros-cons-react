import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { storeInputValue } from './actions';
import * as S from '../Style';

export const Pros = () => {
  const dispatch = useDispatch();

  const mainState = useSelector(state => state.mainState);
  const { initialValuesPros } = mainState;

  return(
    <S.Item>  
      <h2>Pros</h2>
      <S.Wrapper>
        { initialValuesPros && initialValuesPros.map((el, index) => (
          <div key={index}>
            <span>{`${index+1}.`}</span>
            <S.Input 
              value={initialValuesPros[index]}
              key={index}
              onChange={(e) => dispatch(storeInputValue(e, index, initialValuesPros, "pros"))}
            />
          </div>
          ))
        }
      </S.Wrapper>
    </S.Item> 
  )
}
