const initialState = {
  initialValuesPros: [""],
  initialValuesCons:[""]
}

let changeData;
let changeDataName;

export const mainState = (state=initialState, action) => {
  if(action.componentType === "pros"){
    changeData = state.initialValuesPros;
    changeDataName = "initialValuesPros"
  }else if(action.componentType === "cons"){
    changeData = state.initialValuesCons;
    changeDataName = "initialValueCons"
  }
  
  switch (action.type){
    case 'STORE_INPUT_VALUE': { 
      return {
        ...state,
        ...changeData[action.key] = action.data,
      }
    }
    case 'ADD_INPUT': {
      return {
        ...state,
        ...changeData.push(''),
      }
    }
    case 'DELETE_INPUT': {
      if(!action.data.length){
        changeData.splice(action.key,  1); 
      }else{
        changeData[action.key] = action.data
      }
      return {
        ...state,
        [changeDataName]: changeData
      }
    }
    default: {
      return state
    } 
  }
}