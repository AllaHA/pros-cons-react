import styled from 'styled-components';

export const Container = styled.div`
  border: 1px solid grey;
  width: 800px;
  margin: 40px auto;
  h1{
    padding: 18px 12px;
    margin: 0;
    color: #fff;
    background-color: #8f8e8e;
    text-align: center;
  }
`
export const FlexWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`