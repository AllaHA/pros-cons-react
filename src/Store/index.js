import { combineReducers } from 'redux';

import { mainState } from '../Containers/ProsCons/reducer';

export default combineReducers ({
  mainState,
})