import React from 'react';
import * as S from './App.Styles';

import { Pros, Cons } from './Containers/index';

export const App = () => {
  return(
    <S.Container>
      <h1>Should I eat at McDonalds</h1>
      <S.FlexWrapper>
        <Pros/>
        <Cons/>
      </S.FlexWrapper>
    </S.Container>
  )
}

